# GPG

* mac os; install from [www](https://gnupg.org/download/index.html)
* [based on www](https://fuse.pl/beton/krotki-wstep-do-osobistej-kryptografii.html)
* [wiki with more details](https://pl.wikibooks.org/wiki/GnuPG)

`gpg2 -h` manual

`gpg --list-keys`

`gpg2 -e -r email@wp.pl file-name.txt` encrypt

`gpg2 --armour file.JPG.gpg -r mario -r piotr --encrypt file.JPG` example of encrypt file using external public key called `piotr` and internal public key `mario`

`gpg2 --decrypt file-name.txt` decrypt

`gpg2 --import klucz.asc` add external public key to your library