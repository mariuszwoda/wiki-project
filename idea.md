##
# intellij idea

## shortcuts

Based on: [medium.com](https://medium.com/@andrey_cheptsov/top-20-navigation-features-in-intellij-idea-ed8c17075880) & [idea](https://www.jetbrains.com/help/idea/navigation-in-source-code.html)

mac os [win]:

`ctrl+alt+v` create variable from method parameter

`psvm` type and press tab to generate main 

`sout` print ```System.out.println()```

`cmd+e; ctrl+tab` list with last edited files

`cmd+f12` variables and method list

`fn+alt+leftNarrow [alt+home]` navigation bar

`cmd+N [ctrl+N]` class search

`cmd+shift+n [ctrl+shift+n]` file name search

?`[shift+ctrl+alt+N]` wyszukuje nazwy wprowadzonej w całym projekcie

`alt+f1` project view or file structure

`cmd+1 [alt+1]` show/hide project view

`cmd+9 [alt+9]` local changes (version control)

`cmd+shift+T [ctrl+shift+T]` go to tests (& vice versa) or create test for indicated method

`cmd+U [ctrl+U]` goto super class of method given (f.e. toString())

`cmd+alt+B [ctrl+alt+B]` list with implementations of marked method

`cmd+alt+U [ctrl+alt+U]`  diagram for given class

`ctrl+H [?]` hierarchy class

`ctrl+alt+H [?]` hierarchy callers of marked method

`cmd+shift+H [ctrl+shift+H]` hierachy method list

`cmd+B [ctrl+B] (alt+f7 [alt+f7])`  usage of name (method) in all places

`ctrl+G [alt+J] [ctrl+G]` mark and change field occurences

`shift+f5` change field occurences in project/file 

`cmd+shift+/- [ctrl+shift+/-]`  collapse/expand

`cmd+d/y [ctrl+d/y]` duplicate or delete selected line

`alt+shift+narrow` move selected lines

`ctrl+enter [alt+enter]`  generate constructor/javadoc/getters/setters etc

`cmd+p [ctrl+p]` show method parameters

`alt+cmd [ctrl+alt+shift+narrow]` goto last file change

`[alt+narrow]` goto next method

`cmd+4 [ctrl+4]` show log console 

`alt+cmd+l` reformat lines 

`cmd+7` project structure

`cmd+f11 [ctrl+f11]` add to bookmark

`shift+f11` show bookmarks list

`cmd+o` show list with methods to override

`so` start texting `so` in order to print `System.out.printl`

### debug

`cmd+f8 [ctrl+f8]` new breakpoint

`cmd+shift+f8 [ctrl+shift+f8]` breakpoints settings

`shift+f7`  podczas debugowania wybór z listy metody do której zostaniemy przekierowani

## preferences:

* `Editor > General > Smart keys > Insert pair quote/bracket`
* `Editor > General > Appeance > show line numbers`
* `File > Settings > Editor > General > scroll to the Highlight on Caret Movement` [win]
* `Editor > Font > Menlo 10`
* `Apperance & Behaviour > Appearance > Ovverride default font > Menlo 10`
* `File > Other Settings > Default project structure...` default project settings f.e. java version
* `Settings > Editor > General > Editor Tabs` How to increase the maximum number of opened editors in IntelliJ

## error during run spring-boot-app in IDEA

Error (in project [rx-battleships](https://github.com/chrosciu/rx-battleships)): 

`Error: Could not find or load main class com.chrosciu.rxbattleships.RxBattleshipsApplication`

Solution:

* before:
`Run > Edit configuration > Configuration > Use classpath of module > main`

* after:
`Run > Edit configuration > Configuration > Use classpath of module > rx-battleships`



