# git

## github
## creating new repo 
new repo from command line

`mkdir repo`<br>
`git init`<br>
`git status`<br>
`git commit -m 'initial commit'`

then master was created

create new repo in github [site](https://github.com/new)

copy ssh url to git repo (new repo in github)

`git remote add origin ssh://git@github.com/[user]/wiki-github.git`

change to remote

`git remote set-url origin ssh://git@github.com/[user]/wiki-github.git`

verify new remote

`git remote -v`

`git push -u origin master`

## creating new repo at localhost and push to gitlab
`mkdir test-repo` create repo

`git init` create git project

`echo ".idea*" >> .gitignore` add unnecessary files to ignore by git

`echo "*.iml" >> .gitignore`

`git add .` add to git 

`git commit -m 'initial commit'` first commit

`git push --set-upstream git@gitlab.com:[user]/builder-pattern-example.git master` push project to git

## removing empty repo

* If you got error: 
`No source repositories were detected at https://github.com/mariuszwoda/test.git. Please check the URL and try again.`
* choose `begin import`, provide some url to existing repo then click cancel, after than you see 'quick setup' page from where you can remove repo
 

## adding ssh to github
if you want push your changes without providing password, copy ssh public key to github.com

`/Users/[user]/.ssh/id_rsa.pub`

## bitbucket

* How to migrate from BitBucket to GitHub: [www](https://github.com/aiidateam/aiida_core/wiki/How-to-migrate-from-BitBucket-to-GitHub)

clone sources to local

`git clone git@bitbucket.org:[user]/csvreader.git`

## git commands

* new branch and files

`git checkout -b branch-name` new branch

`git add .` add all files to stage

`git add *` add files to stage

`git fetch --all` fetch all remote branches

`git branch -a` show all branches

`git checkout feature` checkout feature branch from remote

* tagging 

`git tag -a "tag-name" -m "tag description"`

`git tag --delete tagname` remove tag from local

`git tag` show tags

`git push origin tag-name`

`git push origin --tags` transfer all of yours tags to remote

* stashing

`git stash list`

`git stash`

`git stash save 'doing-sth'`

`git stash apply`

`git stash apply stash@{0}`

* undo git add

`git reset file-name`

* undo local commit 

`git reset HEAD~1`

* merge remote branch to local feature branch

`git fetch --all` 

`git merge origin/branch-remote`

`git stash save 'tests'` sometimes need to do

`git merge origin/branch-remote` then resole conflicts

`git commit -m 'merge remote to local'` and commit

`git stash apply stash@{0}` then add from stash  

`git commit -m 'all further changes'` and another commit
 
* overwrite local changes

`git fetch --all`

`git reset --hard origin/master`

`git pull origin master`

* remove local branch

`git push --delete remote-name branch-name`

`git branch -d branch-name`

`git branch -D branch-name` if not fully merged

* synchronized with forked master

`git remote -v` check remote conections

`git add upsteram url-to-repo-from-we-forked` once time add remote connection f.e. `git add upsteram https://github.com/original-owner/original-repo.git`

`git fetch upstream`

`git checkout master`

`git merge upstream/master` after that often need to resolve conflicts

* squash 

f.e. three commits as one commit (one unit of code):

`git reset HEAD~3`

`git add *`

`git commit -am "fix bug #14"`

`git push --force` The --force option is needed if the commis have already been pushed, for them to be replaced. This overwrites the previous history!

* others

`echo "# My project's README" >> README.md` add content to README from cli

`git commit -m 'initial commit'`

`echo "idea/" >> .gitignore` add to ignors

`git config --global color.ui auto` colors



`git fetch origin branch-remote` fetching remote branch

`git checkout branch-remote` change branch (no need to use `-d`)

`git commit --amend` add another commit to last commited message`


`git checkout .` remove the changes from local branch and then we can get different branch `git checkout solutions`

