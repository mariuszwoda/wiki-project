# docker

## workshop [stacja-it](https://stacja.it/warsztaty/2018-05-17-docker-budowa-wielokontenerowego-systemu-www-od-podstaw.html), 2018-05-17
K. Murawski, e-point, kmurawski@epoint.pl CEO

### czesc teoretyczna

* LXC - docker wykorzystuje linux conatainers (lxc)
* docker - klient serwer z restowym api
* https://imagelayers.io/ - rysuje warstwy/obrazy z polecenia docker history
* docker posiada dodatkową warstwę R/W która jest ulotna, po restarcie warstwa budowana jest od nowa (this r/w layer)
* domyślny storage driver to aufs
* domyslne rejestry/obrazy: 
    * https://hub.docker.com/_/postgres/ np pobieramy: docker pull postgres (tak jak github z dodatkowymi paczkami)
* mozna tworzyc swoje rejestry lokalne (np zeby ukryc wew firmy)

`docker image prune` kasuje rzeczy niewykorzystywane

`docker image -f -dangling=true` usuwamy wiszace 

`~/Documents/development/workspace/docker/docker-lab [master] $ docker pull redis:4.0.9-alpine` pobiera redisa w danej wersji

`docker ps` pokazuje uruchomine kontenery

`docker exec -it redis sh` uruchomiony shell w danym kontenerze, mozna podpatrzec co jest w srodku //redis to nazwa konternera

`docker rm -f nazwa_kontenera` kill kontener

`docker logs -f hash_konenera` logi w sposob ciagly //--tail 10 ostatnie 10 linii

`docker history postgeres` pokazuje warstwy z jakich sklada sie obraz (dziajacy kontener)

* nieinstalujemy bazy danych na dockerze na PROD, na potrzeby localhost jak najbardziej 

`postgres docker rmi` uswua z dockera calkowicie, rowniez z listy

* alpine: linux specjalnie spreparowany dla dockera, mega zminimalizowana  `docker run -d alpine` 

* https://cloud.docker.com/
* w rejestrze dockerowym mozemy trzymac obrazy lokalnie np

`docker push` dodaje do rejestru

`docker pull` pobiera z rejestru

* nexus stanowi cache repo dla mavena; nexus moze pracowac jako repo dla dockera

####Dockerfile:
* siedzi np w katalogu Dockerfile

`docker build -t test:test .`

* warstwammi sa wykonane pojedyncze polecenia z Dockerfile
* max liczna warstw to 127

`.dockerignor` ktore pliki maja nie byc budowane przy budowaniu

**Przyklad:**
```
FROM ubuntu //dziedziczy z obrazu ubuntu
RUN apt-get update && apt-get install -y vim
COPY
EXPOSE # port na ktorym app ma nasluchiwac
ENTRYPOINT ["echo", "HEllo world"]
```

`docker build -t helloworld .`

`docker history helloworld`

`docker run helloworld` wypisze HEllo world

###czesc praktyczna

`./gradlew clean :yacht-portal:build` zbudowanie gradlem projektu

* z komendy `VOLUME` nie powinniśmy korzystać; tworzy balagan na dysku, lepiej uzywac -v i pobierac z wolumne zewnetrznego
	
`sudo systemctl restart docker` restart dockera

Linkowanie kontenerow:

`docker run -d -p 8080:8080 --name yacht-portal localhost:5000/yacht-portal:1.0.0-with-bmuschko`

`docker run -d -p 80:80 --link yacht-portal apache-local` i w www po `/etc/hosts` powinnien odpalic sie yacht-portal

`docker inspect yacht-portal | less` pokazuje ustawienia dzialajacego portalu

`docker run -v /:/test -it bash` mozna zaatakowac komp pobierajac wszystkie dane: `/`

**Pytania:**
- jaki narzut pamieci podczas dzialania kontenerow, czy jest mozliwosc monitorowania. Tak: [portianer](https://www.portainer.io/overview/)

* trzeba sprawdzac czy docker przy build nie pobiera z cache (-> using cache)

####Docker compose (zamiast z konsoli)

`docker logs -f exercise6_adminer_1`

* zawodowo prowadzacy uzywa [OpenShift origin](https://hub.docker.com/r/openshift/origin/) - trzeba zainstalowac dla siebie

* openshift entrprise - dostepne przez www wszystko juz zainstalowane, mozna podpinac

`influxdb` baza podobna do sql

`telegraf` app oparta o system wtyczek; umozliwia odczyt co okres czasu i zapis do zrodel

`grafana` prezentuje wykresy 

`http://yacht-portal.pl:9000/#/containers` portainer.io


* komendy skopiowane ze slacka:

```
docker exec -it redis sh
docker run -d -p 5000:5000 --name registry registry:2
docker tag postgres localhost:5000/my-postgres:some_version
docker push localhost:5000/my-postgres:some_version
./gradlew clean :yacht-portal:build
~/work/docker-lab/yacht-portal/build/libs$ java -jar yacht-portal.jar
docker rm -f $(docker ps -a -q)
docker rmi -f $(docker images -a -q)
zadanie1 
cd ~/work/docker-lab/yacht-portal
docker build -t yacht-portal:1.0.0 .
docker run -p 8080:8080 yacht-portal:1.0.0

FROM java:8-jdk-alpine
COPY build/libs/yacht-portal.jar yacht-portal.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "yacht-portal.jar"]

kmurawski@kmurawski-lap:~/work/docker-lab/yacht-portal$ docker tag yacht-portal:1.0.0 localhost:5000/yacht-portal:1.0.0
kmurawski@kmurawski-lap:~/work/docker-lab/yacht-portal$ docker images

REPOSITORY                    TAG                 IMAGE ID            CREATED             SIZE
yacht-portal                  1.0.0               aeb85e6aa4b4        4 minutes ago       161MB
localhost:5000/yacht-portal   1.0.0               aeb85e6aa4b4        4 minutes ago       161MB
ubuntu                        latest              452a96d81c30        2 weeks ago         79.6MB
registry                      2                   d1fd7d86a825        4 months ago        33.3MB
java                          8-jdk-alpine        3fd9dd82815c        14 months ago       145MB

kmurawski@kmurawski-lap:~/work/docker-lab/yacht-portal$ docker push localhost:5000/yacht-portal:1.0.0The push refers to a repository [localhost:5000/yacht-portal]

a33d2e3907de: Pushed 
a1e7033f082e: Pushed 
78075328e0da: Pushed 
9f8566ee5135: Pushed 
1.0.0: digest: sha256:5c1aedca08284356ecd82ba5559161dfe69efe3027b2cda832598eb136687594 size: 1159
kmurawski@kmurawski-lap:~/work/docker-lab/yacht-portal$ 

./gradlew clean build :signals:buildImage
./gradlew clean build :signals:buildImage
docker run -v /tmp/data/:/test stacja.it/signals /test
127.0.0.1 yacht-portal.pl
docker run -d -p 8080:8080 --name yacht-portal yacht-portal:1.0.0
docker run -d -p 80:80 --link yacht-portal apache
docker run -d -p 5432:5432 --name db -e "POSTGRES_USER=charters" -e "POSTGRES_PASSWORD=secret" postgres
docker run -d -p 5432:5432 --name db -e "POSTGRES_USER=charters" -e "POSTGRES_PASSWORD=secret" postgres
docker run -d -p 8080:8080 --link db --name spring-boot localhost:5000/yacht-portal:1.0.0-with-bmuschko
docker run -d -p 80:80 --link spring-boot --name apache apache
docker-compose.yaml (yml)
docker-compose up -d (edited)
https://www.youtube.com/watch?v=8XNR3AYRqVw
```

#### po dluzszym czasie - testy localhost 2018-08-01
`docker build -t demo:1.0.0 .` buduje obraz do uruchomienia

`docker run -p 8000:8000 demo:1.0.0` uruchamia kontener na porcie 8000

`docker run -d -p 8000:8000 demo:1.0.0` uruchamia kontener w tle

`docker rm -f demo:1.0.0` kontener kill