# vim 

* `/etc/vim/vimrc` vim setting 
* to colour syntax need to add command  `syntax on` to `~/.vimrc` (create if doesn't exist if root)

`:%s/pass/p/g` find and replace

`vim -O *.txt` open several files

`:ls` opened files list

`:b tab` opened files

`:vs` divide window and show content

`:bn` open next file/window

`:bp` previous file

`/word_to_find` search word

`:noh` disbale marking previous searching word

