# testy-bezpieczenstwa

## workshop [stacja-it](https://stacja.it/warsztaty/2019-03-23-testy-bezpieczenstwa-dla-QA-testerow.html), 2019-03-23
 Mikolaj Kowalczyk

- metody HTTP
- `TRACE` nie powienien byc dostepny na prod

- `botnet` siec polaczonych maszyn, pozwala np na przeprowadzenie atakow ddos, skanowanie sieci
- `owasp` Top10 Web
- `owast testing guide` warto przeczytac, oraz polacal zapoznanie sie z `OSSTMM`

- [Burp Suite](https://portswigger.net/burp/communitydownload) 
    - pierwsza funkcja burpa jest proxy
    - ustawiamy proxy w FF na 127.0.0.1:8040 (reczna konfiguracja)
    - dodajemy do burpa w zakladce proxy adres z FF: 127.0.0.1:8040
    - w zakladce intercept wlaczamy na on `intercept is on` i wchodzimy na jakas strone w FF
    - w request > raw widac odpowiedz
    - instalujemy certyfikat [burpa](http://burp/)
    - `proxy` > `intercept
    
- warto zwrocic uwage w repeater>response>raw na nazwe serwera i na niestandardowe naglowki

- `kali` najlepiej rozwiniety system do testowania bezpieczenstwa (jest np burp wbudowany)
    - applications w kali zawiera szereg programow do testowania
    - np `owasp-zap` podajac url mozemy przeskanowac swoja strone i sprawdzic podatnosc na ataki
      
- `nmap` skaner, rekonesans, rozpoznanie ruchu sieciowego a czego nie widzimy w przegladarce
- metodologie: system `whitebox` wiemy wszystko o testowaniu programu
- `blackbox` probujemy uzyskac dostep do aplikacji nic nie wiedzac np mamy tylko np adres IP

- `wireshark` obserwuje pakiety na sieci po np odpaleniu nmap
 
`nmap scanme.nmap.org` po przeskanowaniu pokaze aktywne porty
`nmap -o scanme.nmap.org`
- w google jak konwertowac raport nmap

- `OpenVas` skaner podatnosci; podajemy cel 
    - `openvas-start` w terminalu w kali uruchomi nam sie openvas; user/pass: admin/admin (ew. nalezy doinstalowac)
    - 127.0.0.1:9392 zakladka scans>tasks
    
- `owasp-zap` spelnia podobne funkcje jak burp jesli uzywamy jako proxy
    - wprowadzamy jakis adres i znajduje ew bledy, generuje raport
    
- bezpiecznik, sekurak i trzeciastrona: 3 najwieksze serwisy bezpieczenstwa w pl
- `C.I.A. Tiad` trzy podstawowy podczas bezpieczenstwa

- `xss` cross site scripting

- nie powinno byc w naglowkach zwracane wersje np Server, Z-Powerded-By, X-AspNet-Version
- np wordpress powinien miec otwarte tylko porty 80 i 8443 i ew jakas baze danych (ew baze ustawic tylko nad dane ip), tymczasem WP ma otwarty np port 22
- `shodan` pozwala skanowac siec z poziomu, pokaze jakie podatnosci wystepuja w skanowanej aplikacji (mozna kupic w blackfriday za 5$); generalnie ciekawy program

`java -jar webwolf-8.0.0.M24.jar [--server.port=8080] [-- server.address=localhost]`

`java -jar webwgoat-8.0.0.M24.jar [--server.port=8080] [-- server.address=localhost]`

`payload` ciag znakow ktory moze wywolac jakis blad np na stronie 
