### main commands

`top -o cpu`, `top -o mem` list processes ordered by cpu or memmory

* add environment variables (mac os): JAVA_HOME and PATH to `~/.bash_profile` in user directory ~ (`/Users/mario`):

    `export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_77.jdk/Contents/Home`
    
    `export PATH=/Users/mario/Documents/development/apache-maven-3.6.0/bin:/Users/mario/.sdkman/candidates/grails/current/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/TeX/texbin`

`/etc/hostname` change username

`uname -r` kernel version (f.e.17.7.0)

`. ./file-name` run some file

`ctrl + shift + t` new tab in console

`ctrl + shift + w` close console tab

`touch file.txt` create file

`./configure --prefix=/home/mario/program_files/name-installing-program` install program to /home/*

`tar czf dane.tar.gz dane` package to tar

`tar tzf dane.tar.gz` show content of tar file

`tar xzf dane.tar.gz` unpack

`gzip` compress archive file *.gz 


`sudo -s` go to sudo user;
`exit` exit sudo 

`sudo apt-get autoremove`
`sudo apt-get autoclean`
`sudo apt-get clean`
remove temporary files in unix

`ssh -l user cornet.if.uj.edu.pl` connect to server with ssh

`shutdown` closes unix

`adduser` add new user

`passwd` change password

`logout` logout from unix

`who`, `users`, `w`, `whoami`, `finger` who is currently logged in

`ruser` wyświetla użytkowników pracujących w systemie

`chmod` zmieniamy parametry pliku

`chown` zmieniamy właściciela pliku

`chgrp` zmieniamy jaka grupa jest właścicielem pliku

* polecenia zwiazane z katalogami 

`ls`  pokazuje nam zawartość katalogu 
`dir`  okrojona wersja ls, pochodząca z msdos'a 
`pwd`  pokazuje nam katalog w którym się znajdujemy 
`cd`  zmieniamy katalog 
`rmdir`  usuwamy katalog 
`mkdir`  nowy katalog 

* polecenia związane z plikami

`cat` edytowanie tekstu 
`rm`  usuwamy plik  
`mv`  przenosimy plik lub zmieniamy jego nazwę 
`cp`  kopiujemy plik 
`mvdir`  przenosimy katalog lub zmieniamy jego nazwę
 
* polecenia związane z procesami

`ps` pokazuje nam jakie procesy są aktualnie wykonywane 
`kill`  "zabijamy" procesy 

`/sbin/iptables -t nat -I PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080` redirect 8080 to 80

`~/../etc/init.d/tomcat7 start` start tomcat
`~/../etc/init.d/tomcat7 stop`

`ps -ef | grep tomcat` search 'for tomcat'

`kill -9 processNumber` kill process with given nb

`ftp ftp.cluster011.hosting.ovh.net` ftp connection to see what's inside

`find . -type f -exec curl --user user:pass --ftp-create-dirs -T {} ftp.cluster011.ovh.net/www/ftptest/{} \;` from local ftptest to remote www/ftptest

`find . -type f -exec curl --user user:pass --ftp-create-dirs -T {} ftp.cluster011.ovh.net/www/{} \;`

`curl -T testfile.txt ftp.cluster011.ovh.net/www/ --user user:pass` local testfile.txt to remote www/

`curl -u user:pass ftp.cluster011.ovh.net/www/testfile.txt -o ~/ftptest/testfile.txt` getFromFtp

### linux version
   
`uname -a` Linux vps390285.ovh.net 3.2.0-4-amd64 #1 SMP Debian 3.2.86-1 x86_64 GNU/Linux

`cat ~/../proc/version` Linux version 3.2.0-4-amd64 (debian-kernel@lists.debian.org) (gcc version 4.6.3 (Debian 4.6.3-14) ) #1 SMP Debian 3.2.86-1

`lsb_release -a` Description: Debian GNU/Linux 7.11 (wheezy)

### console's split
`cmd + d` vertical 

`cmd+shift+d` horizontal 


### console's colors (?)
PS1='\w\[\033[0;32m\]$( git branch 2&gt; /dev/null | cut -f2 -d\* -s | sed "s/^ / [/" | sed "s/$/]/" )\[\033[0m\] \$ '

### tell grep to highlight matches
`export GREP_OPTIONS='--color=auto'`

### tell ls to be colourful
`export CLICOLOR=1`

`export LSCOLORS=Exfxcxdxbxegedabagacad`

### where is java
* mac

`echo $(/usr/libexec/java_home)`

`which java`

* win
`c:\>; for %i in (java.exe) do @echo.; %~$PATH:i$`

### webbapp in tomcat
`cd ~/../var/lib/tomcat7/webapps/`

### open directory from cli
`open .`

### open file from cli f.e. doc
`open -a libreoffice ankieta_java.doc`

### manuals
`man vim` vim manual 

### searching
`find ~/ -name "*vimrc"` /Users/mario//.vimrc

### change sudo password (mac)
`sudo passwd root`

### scp 
`scp sample.war root@vps390285.ovh.net:/var/lib/tomcat7/webapps` send to server

`scp username@hostname:/path/to/remote/file /path/to/local/file` retrieve from server

examples:

skopiowanie pliku lesson1.c z cornet do biezacego katalogu
`scp user@yeti.if.uj.edu.pl:/net/yeti/disk2/user/results_go/2007.07.03/results.out .`

skopiowanie katalogu .1sem z cornet do biezacego katalogu
`scp -r user@yeti.if.uj.edu.pl:/net/yeti/disk2/user/GO_TimeSlotWidth .`

wyslanie pliku sms.cpp na cornet do katalogu glownego
`scp GO_TimeSlotWidth user@yeti.if.uj.edu.pl:/net/yeti/disk2/user/results_go/2007.07.22/`

### curl :

To download you just need to use the basic curl command but add your username and password like this

`curl -u user:pass -o filename.tar.gz ftp://domain.com/directory/filename.tar.gz`

To upload you need to use both the –user option and the -T option as follows.

`curl -u user:pass -T filename.tar.g ftp://domain.com/directory/`

To delete a file from the remote server.

`curl -u username:password -X 'DELE filename.tar.gz' ftp://domain.com/`

### bash examples

````cmd
for timeSlotWidth in 125 150 175 200 225 250 275; do
for LVL1_accept in 0.1; do
for reductionDCB in 0.1; do
for nbTimeStampLVL1 in 5 6 7 8 9 10; do
for seed in 11111; do

#        cat /net/yeti/disk2/woda/panda08.02.15/timeSlotWidth-$timeSlotWidth.nbTimeStampLVL1-$nbTimeStampLVL1.Seed-$seed.out | grep "received L2 decision: 1" | grep "earliestTS:" | awk '{printf("%f\n",$13 )}'> /net/yeti/disk2/woda/panda08.02.15/filter-earliestTS_LVL2.timeSlotWidth-$timeSlotWidth.nbTimeStampLVL1-$nbTimeStampLVL1.Seed-$seed.dat

        cat /net/yeti/disk2/woda/panda08.02.15/timeSlotWidth-$timeSlotWidth.nbTimeStampLVL1-$nbTimeStampLVL1.Seed-$seed.out | grep "detector_25" | grep "ealiestTS:" | grep "controTS:" | grep "L1lat:" | awk '{printf("%d\n",$12 )}'> /net/yeti/disk2/woda/panda08.02.15/filter-earliestTS_LVL1.timeSlotWidth-$timeSlotWidth.nbTimeStampLVL1-$nbTimeStampLVL1.Seed-$seed.dat

        done;
        done;
        done;
   done;
   done;
````

````cmd
for size in 8 16 24 ; do

    cat /net/yeti/disk2/korcyl/results_tight_ConcentrFilter_0.07_TimeSlot200_4CPUfarm1.40usL1Time.L1farmSize.L1NON_SHARED.$size.out | grep "L3lat" | grep "detector_0"| awk '{printf("%f %f\n",$2,$14)}' > /net/yeti/disk2/korcyl/L3Latency_tight_ConcentrFilter_0.07_TimeSlot200_4CPUfarm1.40usL1Time.L1farmSize.L1NON_SHARED.dat.$size

    cat /net/yeti/disk2/korcyl/results_tight_ConcentrFilter_0.07_TimeSlot200_4CPUfarm1.40usL1Time.L1farmSize.L1NON_SHARED.$size.out | grep "L2lat" | grep "detector_0"| awk '{printf("%f %f\n",$2,$13)}' > /net/yeti/disk2/korcyl/L2Latency_tight_ConcentrFilter_0.07_TimeSlot200_4CPUfarm1.40usL1Time.L1farmSize.L1NON_SHARED.dat.$size

    cat /net/yeti/disk2/korcyl/results_tight_ConcentrFilter_0.07_TimeSlot200_4CPUfarm1.40usL1Time.L1farmSize.L1NON_SHARED.$size.out | grep "L1lat" | grep "detector_0"| awk '{printf("%f %f\n",$2,$16)}'> /net/yeti/disk2/korcyl/L1Latency_tight_ConcentrFilter_0.07_TimeSlot200_4CPUfarm1.40usL1Time.L1farmSize.L1NON_SHARED.dat.$size

    done;
````

````cmd
for size in 8 16 24 ; do
    cat /net/yeti/disk2/woda/testFullSize.out | grep " L1lat: " | grep "detector_0"| awk '{printf("%f %f\n",$2,$8)}'> /net/yeti/disk2/woda/Test1.dAt
    done;
````

````cmd
for size in 8 16 24 ; do
    cat /net/yeti/disk2/korcyl/results_tight_ConcentrFilter_0.07_TimeSlot200_4CPUfarm1.40usL1Time.L1farmSize.$size.out | grep "effectiveProceesingTime:"| awk '{printf("%f %f\n",$2,$10)}' > /net/yeti/disk2/korcyl/L1LatencyAtProcessor_tight_ConcentrFilter_0.07_TimeSlot200_4CPUfarm1.40usL1Time.L1farmSize.dat.$size
    done;
````
````cmd
for size in 8 16 24 ; do

cat /net/yeti/disk2/korcyl/results_tight_ConcentrFilter_0.07_TimeSlot200_4CPUfarm1.40usL1Time.L1farmSize.L1_SHARED_NEW.debug1.$size.out | grep "detector_0" | grep "FARM1ACKMESSAGE" | awk 'BEGIN{pa = 0; pi=0}{if(pa>0){printf("%d %d\n",$2-pa, $14-pi)} pa = $2; pi = $14}' > /net/yeti/disk2/korcyl/results_tight_ConcentrFilter_0.07_TimeSlot200_4CPUfarm1.40usL1Time.L1farmSize.L1_SHARED_NEW.debug1.dat.$size

done;
````

### go examples

```cmd
date
for acceptRate in 1 2 3 4 5 6 7 8 9; do
     rm ./run.x
     logname=results_L1acceptRate_0.$acceptRate.L1accept_.out
     cat ./farmProcessor1.cpp.orig | sed "s/if ( random.Uni() < 0.5 )/if ( random.Uni() < 0.$acceptRate )/" > ./farmProcessor1.cpp

     touch ./farmProcessor1.cpp
     make
    
     echo "modeling time 2 sec" > $logname
     echo "**************************" >> $logname
     cat ./detectorBuffer.h >> $logname
     echo "**************************" >> $logname
     cat ./detectorBuffer.cpp >> $logname
     echo "**************************" >> $logname
     cat ./farmProcessor1.h >> $logname
     echo "**************************" >> $logname
     cat ./farmProcessor1.cpp >> $logname
     echo "**************************" >> $logname
     cat ./farmProcessor2.h >> $logname
     echo "**************************" >> $logname
     cat ./farmProcessor2.cpp >> $logname
     echo "**************************" >> $logname
     cat ./farmProcessor3.h >> $logname
     echo "**************************" >> $logname
     cat ./farmProcessor3.cpp >> $logname

     ./run.x 2000000000 >> $logname
     mv $logname /net/yeti/disk2/korcyl/
done;
date
```

### awk example

filterSingleProcessor.awk:

````cmd
BEGIN {
}

{
   timeStampOrg = int($8);
   timeStamp = int( timeStampOrg / 8);
   processor = int(timeStamp % 8);
   printf(" timeStamp: %d; processor: %d\n", timeStamp, processor);
   if ( processor == 1)
   {
       printf("%f %f\n",$2,$16);
   }
}
END {
}
````

### bash-exim.sh
```
#cd /c/exim/gerrit.int.com.pl
cd "c:\Temp\exim"
#eval $(ssh-agent -s)
#ssh-add
bash
```

### bash-exim.bat
```
@ECHO OFF
cd C:\
cd "Program Files (86x)"
cd Git
cd bin
sh.exe -l -i -c "bash c:/users/bash-exim.sh"
```